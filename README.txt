ABOUT
-----

This module provides GoPay integration for the Payment platform. 
GoPay is a Czech payment service provider. With this module the 
sites owner will be available to enable customers to pay with this 
type of payment.


GENERAL CONFIGURATION
---------------------

1. Register your shop on https://gopay.com and wait until you receive 
   the necessary authorizing data.
2. Download and install this module.
3. Go to Configuration > Web Services > Payment > Payment Methods 
   > Add payment method > GoPay 
   (admin/config/services/payment/method/add/GoPayPaymentMethodController).
4. Fill authorizing data and select payment gates you want to have 
   available in your shop.
5. Set API URL to sandbox gateway and provide some tests.
6. If everything works, set gateway to production.
